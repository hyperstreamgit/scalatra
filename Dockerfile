FROM tomcat
MAINTAINER Lina
RUN apt-get update && apt-get clean
COPY target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/app.war
EXPOSE 8080
CMD ["catalina.sh", "run"]